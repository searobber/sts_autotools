package common

import (
	"bytes"
	"fmt"
	"log"
	"os"
	"os/signal"
	"runtime"
	"syscall"

	_ "net/http/pprof"
)

func PrintMemStats(memo string) {
	// logger.SetFlags(log.Ldate | log.Lmicroseconds)
	var m runtime.MemStats
	runtime.ReadMemStats(&m)
	log.Println(memo)
	// logger.Info(memo)
	fmt.Printf("%s Alloc = %v TotalAlloc = %v Sys = %v NumGC = %v\n", memo, m.Alloc/1024, m.TotalAlloc/1024, m.Sys/1024, m.NumGC)
	// logger.Info("%s Alloc = %v TotalAlloc = %v Sys = %v NumGC = %v\n", memo, m.Alloc/1024, m.TotalAlloc/1024, m.Sys/1024, m.NumGC)
}

func GracefullExit() {
	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM, syscall.SIGINT)
	go func() {
		<-c
		fmt.Println("Gracefull Exit application...")
		// Your cleanup tasks go here
		// db.Close()
		// redisConn.Close()
		os.Exit(0)
	}()
}

func NormalExit() {
	fmt.Println("Exit application...")
	os.Exit(0)
}

// dropCR drops a terminal \r from the data.
func dropCR(data []byte) []byte {
	if len(data) > 0 && data[len(data)-1] == '\r' {
		return data[0 : len(data)-1]
	}
	return data
}

func ScanCRLF(data []byte, atEOF bool) (advance int, token []byte, err error) {
	if atEOF && len(data) == 0 {
		return 0, nil, nil
	}
	if i := bytes.Index(data, []byte{'\r', '\n'}); i >= 0 {
		// We have a full newline-terminated line.
		return i + 2, dropCR(data[0:i]), nil
	}
	// If we're at EOF, we have a final, non-terminated line. Return it.
	if atEOF {
		return len(data), dropCR(data), nil
	}
	// Request more data.
	return 0, nil, nil
}
