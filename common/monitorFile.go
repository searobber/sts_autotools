package common

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"

	"github.com/fsnotify/fsnotify"
	"github.com/google/logger"
	"github.com/k0kubun/pp"

	_ "net/http/pprof"
)

var logPath = "./OpenCloseServerRecords.log"
var logfile *os.File

func GetLogFile() *os.File {
	return logfile
}

type monitor struct {
}

var moni *monitor

func GetMonitor() *monitor {
	if moni == nil {
		moni = &monitor{}
	}
	return moni
}

func (self monitor) Run(configArray map[string]map[string]string) {
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		log.Fatal(err)
	}
	defer watcher.Close()

	done := make(chan bool)
	go func() {
		for {
			select {
			case event, ok := <-watcher.Events:
				if !ok {
					return
				}
				log.Println("event:", event)
				if event.Op&fsnotify.Write == fsnotify.Write {
					//目前並沒有重新呼叫的需求，這個監測檔案只是作為一個示例放著
					configArray = LoadFile(configArray)
					// log.Println("目前並沒有重新呼叫的需求，這個監測檔案只是作為一個範例")
					log.Println("modified file:", event.Name)
					// pp.Println(configArray)
				}
			case err, ok := <-watcher.Errors:
				if !ok {
					return
				}
				log.Println("error:", err)
			}
		}
	}()

	err = watcher.Add("config.json")
	if err != nil {
		log.Fatal(err)
	}
	<-done
}

func LoadFile(configArray map[string]map[string]string) map[string]map[string]string {
	// configArray = make(map[string]interface{})
	configFile, _ := os.Open("config.json")
	defer configFile.Close()
	cmdTxtFile, e := ioutil.ReadAll(configFile)
	configArray = nil
	if e != nil {
		pp.Printf("config File read error: %v\n", e)
	}
	json.Unmarshal(cmdTxtFile, &configArray)
	pp.Println("config.json load finish.")
	// pp.Println(configArray)
	var lerr error
	logfile, lerr = os.OpenFile(logPath, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0660)
	if lerr != nil {
		logger.Fatalf("Failed to open log file: %v", lerr)
	}
	return configArray
}
