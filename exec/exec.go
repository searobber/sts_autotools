package exec

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"

	"github.com/google/logger"
	"github.com/k0kubun/pp"
	"github.com/marcusolsson/tui-go"
	"github.com/shirou/gopsutil/process"

	"net/http"
	_ "net/http/pprof"
	"net/url"

	common "9splay.com/sts/autotools/common"
)

const (
	cLR0 = "\x1b[30;1m"
	cLRR = "\x1b[31;1m"
	cLRG = "\x1b[32;1m"
	cLRY = "\x1b[33;1m"
	cLRB = "\x1b[34;1m"
	cLRM = "\x1b[35;1m"
	cLRC = "\x1b[36;1m"
	cLRW = "\x1b[37;1m"
	cLRN = "\x1b[37;0m"
)

var configArray map[string]map[string]string

var ResetCount = 0

// var logfile *os.File

func init() {
	log.SetFlags(log.LstdFlags | log.Lmicroseconds)

	common.PrintMemStats("start load file to safe array")

	configArray = common.LoadFile(configArray)

	common.PrintMemStats("end load file to end array")

	go common.GetMonitor().Run(configArray)
}

// func LoadFile() {
// 	// configArray = make(map[string]interface{})
// 	configFile, _ := os.Open("config.json")
// 	defer configFile.Close()
// 	cmdTxtFile, e := ioutil.ReadAll(configFile)
// 	configArray = nil
// 	if e != nil {
// 		pp.Printf("config File read error: %v\n", e)
// 	}
// 	json.Unmarshal(cmdTxtFile, &configArray)
// 	pp.Println("config.json load finish.")
// 	// pp.Println(configArray)
// 	var lerr error
// 	logfile, lerr = os.OpenFile(logPath, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0660)
// 	if lerr != nil {
// 		logger.Fatalf("Failed to open log file: %v", lerr)
// 	}
// }

func startLog() {

}

func getRandomTime() int {
	rand.Seed(time.Now().UnixNano())
	return rand.Intn(10)
}

func Terminates() {
	hostName, _ := os.Hostname()
	SendLineNotifyMessage("在 " + hostName + " " + localhostIpAddress() + " 上啟動 [一鍵開關] 失敗，請查看是不是有admin權限")
	os.Exit(3)
}

func Run() {
	defer common.GetLogFile().Close()
	defer logger.Init("Logger", true, true, common.GetLogFile()).Close()
	logger.SetFlags(log.Ldate | log.Ltime | log.Lshortfile)
	logger.Info("執行環境準備完成，請輸入需要執行的指令。")

	common.GracefullExit()

	printWelcome()

	// pp.Println(configArray)

	reader := bufio.NewReader(os.Stdin)
	scanner := bufio.NewScanner(reader)
	scanner.Split(common.ScanCRLF)
	for scanner.Scan() {
		// fmt.Printf("%s\n", scanner.Text())
		switch scanner.Text() {
		case "1":
			//這裡依關機順序關閉相關的SERVER，順序可以過config.json裡來設定，開機那裡也是一樣。
			fmt.Println("關閉GameServer")
			for key, _ := range configArray["GameServer"] {
				CloseProcess(key)
			}
			fmt.Println("關閉CrossGameServer")
			for key, _ := range configArray["CrossGameServer"] {
				CloseProcess(key)
			}
			fmt.Println("關閉GWBS")
			for key, _ := range configArray["GWBS"] {
				CloseProcess(key)
			}
			fmt.Println("關閉NGS")
			for key, _ := range configArray["NGS"] {
				CloseProcess(key)
			}
			fmt.Println("關閉DBIS")
			for key, _ := range configArray["DBIS"] {
				CloseProcess(key)
			}
			printWelcome()
		case "2":
			fmt.Println("啟動DBIS")
			for key, value := range configArray["DBIS"] {
				StartProcess(key, value)
			}
			fmt.Println("啟動NGS")
			for key, value := range configArray["NGS"] {
				StartProcess(key, value)
			}
			fmt.Println("啟動GWBS")
			for key, value := range configArray["GWBS"] {
				StartProcess(key, value)
			}
			fmt.Println("啟動CrossGameServer")
			for key, value := range configArray["CrossGameServer"] {
				StartProcess(key, value)
			}
			fmt.Println("啟動GameServer")
			for key, value := range configArray["GameServer"] {
				StartProcess(key, value)
			}
			printWelcome()
		case "3":
			CheckProcessStatus("GameServer.exe")
		case "q":
			common.NormalExit()
		default:
			fmt.Println("指令error")
		}
	}
}

func RunGui() {
	defer common.GetLogFile().Close()
	defer logger.Init("Logger", false, true, common.GetLogFile()).Close()
	logger.SetFlags(log.Ldate | log.Ltime | log.Lshortfile)
	logger.Info("[TGui]執行環境準備完成，請輸入需要執行的指令。")
	status := tui.NewStatusBar("執行環境準備完成，請輸入需要執行的指令。")

	window := tui.NewVBox(
		tui.NewPadder(8, 1, tui.NewLabel("===========[請輸入對應的功能鍵]===========")),
		tui.NewPadder(8, 1, tui.NewLabel("(1)停止所有的Server  |  (2)開啟所有Server")),
		tui.NewPadder(8, 1, tui.NewLabel("(a)開啟自動滾到底    |  (s)關閉自動滾到底")),
		tui.NewPadder(8, 1, tui.NewLabel("(q/esc)離開程式      |  (上下鍵)上下捲")),
		tui.NewPadder(8, 1, tui.NewLabel("==========================================")),
	)
	window.SetBorder(true)

	history := tui.NewVBox()
	historyScroll := tui.NewScrollArea(history)
	historyScroll.SetAutoscrollToBottom(true)

	historyBox := tui.NewVBox(historyScroll)
	historyBox.SetBorder(true)

	wrapper := tui.NewVBox(
		tui.NewSpacer(),
		window,
		historyBox,
		tui.NewSpacer(),
	)
	content := tui.NewHBox(tui.NewSpacer(), wrapper, tui.NewSpacer())

	root := tui.NewVBox(
		content,
		status,
	)

	ui, err := tui.New(root)
	if err != nil {
		log.Fatal(err)
	}

	ui.SetKeybinding("Up", func() { historyScroll.Scroll(0, -1) })
	ui.SetKeybinding("Down", func() { historyScroll.Scroll(0, 1) })
	ui.SetKeybinding("a", func() { historyScroll.SetAutoscrollToBottom(true) })
	ui.SetKeybinding("s", func() { historyScroll.SetAutoscrollToBottom(false) })
	ui.SetKeybinding("Esc", func() { ui.Quit() })
	ui.SetKeybinding("Q", func() { ui.Quit() })
	ui.SetKeybinding("1", func() {
		// fmt.Println("關閉GameServer")
		history.Append(tui.NewHBox(tui.NewLabel("關閉GameServer."), tui.NewSpacer()))
		for key, _ := range configArray["GameServer"] {
			CloseProcess(key)
		}
		// fmt.Println("關閉CrossGameServer")
		history.Append(tui.NewHBox(tui.NewLabel("關閉CrossGameServer."), tui.NewSpacer()))
		for key, _ := range configArray["CrossGameServer"] {
			CloseProcess(key)
		}
		// fmt.Println("關閉GWBS")
		history.Append(tui.NewHBox(tui.NewLabel("關閉GWBS."), tui.NewSpacer()))
		for key, _ := range configArray["GWBS"] {
			CloseProcess(key)
		}
		// fmt.Println("關閉NGS")
		history.Append(tui.NewHBox(tui.NewLabel("關閉NGS."), tui.NewSpacer()))
		for key, _ := range configArray["NGS"] {
			CloseProcess(key)
		}
		// fmt.Println("關閉DBIS")
		history.Append(tui.NewHBox(tui.NewLabel("關閉DBIS."), tui.NewSpacer()))
		for key, _ := range configArray["DBIS"] {
			CloseProcess(key)
		}
	})

	ui.SetKeybinding("2", func() {
		// fmt.Println("啟動DBIS")
		history.Append(tui.NewHBox(tui.NewLabel("啟動DBIS."), tui.NewSpacer()))
		for key, value := range configArray["DBIS"] {
			StartProcess(key, value)
		}
		// fmt.Println("啟動NGS")
		history.Append(tui.NewHBox(tui.NewLabel("啟動NGS."), tui.NewSpacer()))
		for key, value := range configArray["NGS"] {
			StartProcess(key, value)
		}
		// fmt.Println("啟動GWBS")
		history.Append(tui.NewHBox(tui.NewLabel("啟動GWBS."), tui.NewSpacer()))
		for key, value := range configArray["GWBS"] {
			StartProcess(key, value)
		}
		// fmt.Println("啟動CrossGameServer")
		history.Append(tui.NewHBox(tui.NewLabel("啟動CrossGameServer."), tui.NewSpacer()))
		for key, value := range configArray["CrossGameServer"] {
			StartProcess(key, value)
		}
		// fmt.Println("啟動GameServer")
		history.Append(tui.NewHBox(tui.NewLabel("啟動GameServer."), tui.NewSpacer()))
		for key, value := range configArray["GameServer"] {
			StartProcess(key, value)
		}
	})

	if err := ui.Run(); err != nil {
		log.Fatal(err)
	}
}

func printWelcome() {
	fmt.Println("===========[請輸入對應的功能鍵]===========")
	fmt.Println("(1)停止所有的Server")
	fmt.Println("(2)開啟所有Server")
	fmt.Println("(q)離開程式")
	fmt.Println("★ 可以帶入參數 -m gui 啟動GUI模式")
	fmt.Println("==========================================")
}

func TimerAlert(monitorApplicationName string) {
	ticker := time.NewTicker(1 * 10 * time.Second)
	for _ = range ticker.C {
		defer ticker.Stop()
		timestamp := time.Now().Unix()
		tm := time.Unix(timestamp, 0)
		pp.Println(tm.Format("2006-01-02 15:04:05"))
		CheckProcessStatus(monitorApplicationName)
	}
}

func CheckProcessStatus(appName string) {
	procs, _ := process.Processes()
	for i := 0; i < len(procs); i++ {
		cmdstr, _ := procs[i].Cmdline()
		pp.Println(cmdstr, procs[i].Pid)
		// if cmdstr == appName {
		// 	cmdName, _ := procs[i].Name()
		// 	pp.Println(cmdstr, cmdName, procs[i].Pid)
		// }
	}
}

func CloseProcess(appName string) {
	procs, _ := process.Processes()
	for i := 0; i < len(procs); i++ {
		cmdstr, _ := procs[i].Cmdline()
		// pp.Println(cmdstr, appName)
		if cmdstr == appName {
			cmdName, _ := procs[i].Name()
			// pp.Println(cmdstr, cmdName, procs[i].Pid)
			logger.Info("[關閉]"+cmdstr+" [", cmdName+"] ", procs[i].Pid)
			procs[i].Terminate()
		}
	}
}

func StartProcess(appName string, applink string) {
	//檢查是不是有正在執行的程式，如果已經在執行了就什麼都不作，如果沒有則就執行
	procs, _ := process.Processes()
	count := 0
	for i := 0; i < len(procs); i++ {
		cmdstr, _ := procs[i].Cmdline()
		if cmdstr == appName {
			cmdName, _ := procs[i].Name()
			logger.Info("[重覆]"+cmdstr+" [", cmdName+"] ", procs[i].Pid)
			logger.Info("[重覆]"+cmdName+" ", procs[i].Pid, " 己經啟動!!!")
			count++
		}
	}
	if count == 0 {
		checkFileExist := FileExist(applink)
		if checkFileExist {
			cmd, _ := exec.Command("powershell", applink).Output()
			output := string(cmd[:])
			if output == "" {
				logger.Info("[開啟]"+applink+" ", "啟動成功!!!")
			}
		}
		// pp.Println(output)
	}
}

func FileExist(path string) bool {
	_, err := os.Lstat(path)
	return !os.IsNotExist(err)
}

func SendLineNotifyMessage(msg string) {
	// token := "3F9C89wkQ7JKuHTYdzetmVfk6LxdMxNiNPzbDemkTli"
	client := &http.Client{}
	r := &http.Request{}
	data := url.Values{}
	data.Add("message", "\n"+msg)
	r, _ = http.NewRequest("POST", "https://notify-api.line.me/api/notify", strings.NewReader(data.Encode()))
	r.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	r.Header.Add("Content-Length", strconv.Itoa(len(data.Encode())))
	r.Header.Add("Authorization", fmt.Sprintf("Bearer %s", configArray["notifyAccessToken"]))

	resp, err := client.Do(r)

	if err != nil {
		log.Println("er:", err)
	} else {
		body, _ := ioutil.ReadAll(resp.Body)
		pp.Println(string(body))
	}
}

func localhostIpAddress() string {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	for _, address := range addrs {
		if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				fmt.Println(ipnet.IP.String())
				return ipnet.IP.String()
			}
		}
	}
	return ""
}

// ======這段是輸出到畫面上CONSOLE時的方法======
// cmd := exec.Command("cmd.exe")
// cmd.Stdout = os.Stdout
// input, _ := cmd.StdinPipe()
// cmd.Start()
// fmt.Fprintln(input, "dir")
// fmt.Fprintln(input, "exit")
// cmd.Wait()
// ======這段是輸出到畫面上CONSOLE時的方法======
