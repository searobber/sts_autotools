# STS Autotools

一鍵開關server工具。

### 準備工作

0.請和之前一樣要建立好server，包括捷徑的建立都是必要的工作

1.修改config.json檔，請參考目前server上的檔案路徑進行修改並且要填好link的路徑

2.開始測試。

### 邏輯

1.一鍵開關的程式是基於原來操作服務器的方法作的自動化工具，要注意開關服務器的順序(目前己經將順序寫死在程式中，無法被修改，但請務必一定要填好config.json裡的設定，不要把跨服服務器設到GS的那裡去就不會出錯)

2.一鍵程式只是幫忙解決繁鎖的開關服務器問題，對服務器本身的問題並無法加以改善。

3.還是需要人工進行服務器的初始設定。

4.可再加裝web服務作成可以使用jenkins進行一鍵開關，但目前把這項功能移除避免失手。

### 特殊設定

1.有作防漏開功能，在啟動server時如果有啟動失敗的情況發生時，不需要關掉啟動的服務器，只需要再執行一次一鍵啟動，再次執行時會去檢查如果已經有啟動過的服務器不會再次啟動，但未啟動的服務器則會被觸發啟動。所以一鍵啟動有漏開時請勿緊張，再按一次就好可以了。

2.有可愛的GUI模式可以用.... ;-)  

[<img src="https://64.media.tumblr.com/817d3d6aabf8d9b36c5f367edb97b973/9413aa6fc2d2d78f-7f/s1280x1920/d271cc27af795258c2782d29e7ca6d7837b314e7.png">](https://64.media.tumblr.com/817d3d6aabf8d9b36c5f367edb97b973/9413aa6fc2d2d78f-7f/s1280x1920/d271cc27af795258c2782d29e7ca6d7837b314e7.png)

```
mac產出windows執行檔範例
CGO_ENABLED=0 GOOS=windows GOARCH=amd64 go build -o bin/EasyWeb-win64.v2.exe main.go 
```
