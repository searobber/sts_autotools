package main

import (
	"flag"
	"fmt"
	"os"
	"runtime"

	"9splay.com/sts/autotools/exec"
)

// var mutex sync.RWMutex

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU() * 1)
	os := runtime.GOOS

	var flagString string
	flag.StringVar(&flagString, "m", "", "choice mode.")
	flag.Parse()

	switch os {
	case "windows":
		fmt.Println("current operating system: Windows")
		if !amAdmin() {
			fmt.Println("[!!] 需要 administrator 權限才能執行此程式 !!!!!!!!!!!!")
			exec.Terminates()
		}
		fmt.Println("current operating system: Windows")
	case "darwin":
		fmt.Println("current operating system: MAC operating system")
	case "linux":
		fmt.Println("current operating system: Linux")
	default:
		fmt.Printf("current operating system: %s.\n", os)
	}

	switch flagString {
	case "gui":
		exec.RunGui()
	default:
		exec.Run()
	}
}

func amAdmin() bool {
	_, err := os.Open("\\\\.\\PHYSICALDRIVE0")
	if err != nil {
		fmt.Println("admin no")
		return false
	}
	fmt.Println("admin yes")
	return true
}
