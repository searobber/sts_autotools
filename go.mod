module 9splay.com/sts/autotools

go 1.17

require (
	github.com/fsnotify/fsnotify v1.5.1
	github.com/google/logger v1.1.1
	github.com/k0kubun/pp v3.0.1+incompatible
	github.com/shirou/gopsutil v3.21.9+incompatible
)

require (
	github.com/StackExchange/wmi v1.2.1 // indirect
	github.com/andybalholm/brotli v1.0.2 // indirect
	github.com/gdamore/encoding v1.0.0 // indirect
	github.com/gdamore/tcell v1.4.0 // indirect
	github.com/go-ole/go-ole v1.2.5 // indirect
	github.com/gofiber/fiber/v2 v2.20.2 // indirect
	github.com/klauspost/compress v1.13.4 // indirect
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
	github.com/marcusolsson/tui-go v0.4.0 // indirect
	github.com/mattn/go-colorable v0.1.11 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/mattn/go-runewidth v0.0.13 // indirect
	github.com/mitchellh/go-wordwrap v1.0.1 // indirect
	github.com/nsf/termbox-go v1.1.1 // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	github.com/stefantds/csvdecoder v0.2.0 // indirect
	github.com/tklauser/go-sysconf v0.3.9 // indirect
	github.com/tklauser/numcpus v0.3.0 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasthttp v1.29.0 // indirect
	github.com/valyala/tcplisten v1.0.0 // indirect
	golang.org/x/sys v0.0.0-20211025201205-69cdffdb9359 // indirect
	golang.org/x/text v0.3.7 // indirect
)
